package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"
)

// Movie with all its attributes.
type Movie struct {
	ID, Title, ReleaseYear, PosterURL string
	Actors, Genres, Regisseurs        []string
	SimilarMovies                     []Movie
}

// Webserver consists of basic config information and two processing components,
// namly a Retriever and a Parser
type Webserver struct {
	config    Config
	retriever *Retriever
	parser    *Parser
}

// NewWebserver creates a Webserver and initializes its components
func NewWebserver(config Config) (*Webserver, error) {
	return &Webserver{
		config,
		NewRetriever(config.QueryURL),
		NewParser(config.SearchFullDOM),
	}, nil
}

// Defines handling function and starts listening on port.
func (s *Webserver) startWebserver() {
	// TODO: enable or shut down TLS explicitly
	fmt.Println("Webserver listening on", s.config.GetPortString())
	http.Handle(s.config.ServingPath, s)
	err := http.ListenAndServe(s.config.GetPortString(), nil)
	if err != nil {
		log.Println("Could not start webserver.")
	}
}

// ServeHTTP implements http.Handler interface.
// Gets called when client requests page for s.config.ServingPath
func (s *Webserver) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println("Request received.")

	// read request
	id := s.getMovieID(r.URL) // extract movieID, e.g. "B00K19SD8Q"

	// handle request
	response := s.handleRequest(w, r, id)

	// set correct MIME-type in response to match JSON data
	w.Header().Set("Content-Type", s.config.MIMEType)

	// send response
	w.Write(response)

	log.Println("Response sent.\n-----")
}

// handleRequest gets called for every request from client.
func (s *Webserver) handleRequest(w http.ResponseWriter, r *http.Request, id string) []byte {
	// Retrieve HTML from data source
	retrievedData, err := s.retriever.Retrieve(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return nil
	}

	// Parse HTML into Movie object
	movie, err := s.parser.Parse(retrievedData)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return nil
	}
	s.parser.movie.ID = id // asign movieId to also show it in the result

	// encode Movie into JSON
	jsonData, err := movie.toJSON()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return nil
	}
	return jsonData
}

// getMovieId extracts the movie-ID from last part of the URL.
func (s *Webserver) getMovieID(url *url.URL) string {
	// Sublicing to extract movie-ID
	id := url.Path[len(s.config.QueryURL.Host)+1 : len(url.Path)]
	log.Println("Requested Amazon Movie ID:", id)
	return id
}

// toJSON converts struct Movie into JSON format.
func (m *Movie) toJSON() ([]byte, error) {
	//bytes, err := json.Marshal(m)
	bytes, err := json.MarshalIndent(m, "", "  ")
	if err != nil {
		return bytes, errors.New("ToJSON error")
	}
	return bytes, nil
}
