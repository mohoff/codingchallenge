package main

import (
	"io/ioutil"
	"log"
	"testing"
)

/*

  Benchmark Findings:
  -------------------
  It takes ~1.3s to parse and process all nodes (by calling processAllNodes()).
  And it takes ~1.4s to parse and process my custom subset of nodes (by calling
  processNodes()). Why is that? The processNodes() way should be way faster
  because of the strongly reduced subset of nodes which needs to be traversed.
  However this might be slowed down by copying lots of nodes to traverse the
  DOM.

*/

func BenchmarkParse(b *testing.B) {
	for n := 0; n < b.N; n++ {
		// sample.html contains the response from http://www.amazon.de/gp/product/B00KY1U7GM
		body, err := ioutil.ReadFile("./html-sample/sample.html")
		if err != nil {
			log.Println("Could not read sample file.")
			return
		}

		p := NewParser(true)
		p.Parse(string(body))
		//Parse("1", string(body)) // "1" as test movie-ID
	}
}
