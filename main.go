package main

import (
	"fmt"
)

// Entry point for the app.
func main() {
	fmt.Println("App started.")

	// Create new config.
	config, err := NewConfig(
		8080,
		"/movie/amazon/",
		"application/json",
		"http://www.amazon.de/gp/product/",
		true,
	)
	if err != nil {
		fmt.Println(err.Error())
	}

	// Create new Webserver and pass the config.
	w, err := NewWebserver(*config)
	if err != nil {
		fmt.Println("Failed to start Webserver")
	}
	// Start the Webserver and listen on specified port and path.
	w.startWebserver()

	// for debugging purposes
	/*
		id, retrievedData, err := Retrieve("B00KY1U7GM")
		if err != nil {
			//http.Error(w, err.Error(), http.StatusBadRequest)
		}
		log.Println("Retrieve finished.")
		parsingResult, err := Parse(retrievedData, id)
		if err != nil {
			//http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		log.Println("Parsing finished.")
		jsonData, err := ToJSON(parsingResult)
		if err != nil {
			//http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		log.Println("JSONify finished.")
		fmt.Printf("%s\n", jsonData)
	*/
}
