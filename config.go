package main

import (
	//"net/url"
	"errors"
	"net/url"
	"strconv"
)

// Config holds global settings for the Webserver and all its components
type Config struct {
	Port                  int
	ServingPath, MIMEType string
	QueryURL              *url.URL
	SearchFullDOM         bool
}

// NewConfig creates a new Config and parses URL-string into url.URL
func NewConfig(port int, servingPath string, MIMEType string, queryURL string, searchFullDOM bool) (*Config, error) {
	if url, err := url.Parse(queryURL); err == nil {
		return &Config{
			Port:          port,
			ServingPath:   servingPath,
			MIMEType:      MIMEType,
			QueryURL:      url,
			SearchFullDOM: searchFullDOM,
		}, nil
	}
	return nil, errors.New("Could not parse queryURL.")
}

// Converts numeric port into string format with prepended colon.
func (c *Config) GetPortString() string {
	if c.Port != 0 {
		return ":" + strconv.Itoa(c.Port)
	}
	return ""
}

// Returns the URL as string
func (c *Config) GetURLString() string {
	return c.QueryURL.String()
}
