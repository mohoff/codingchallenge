/*
Assuming Amazon uses a CMS that won't change HTML structure and naming of tags an attributes,
we can use static references to access desired values --> processNodes().
This approach should be faster than full DOM traversal --> processAllNodes().
NOTE: But it is not faster, as you can see in parser_test.go

As a third alternative: could have used GoQuery from
https://github.com/PuerkitoBio/goquery . Because of performance reasons
(although I can't say if it matters at all) I chose the DIY approach.
*/

package main

import (
	"errors"
	"golang.org/x/net/html"
	"log"
	"reflect"
	"strings"
)

// Parser which contains a Movie which gets populated during parsing.
type Parser struct {
	searchFullDOM bool
	movie         *Movie
}

// NewParser creates a Parser
// Based on searchFullDOM a different processing method is chosen (see below)
func NewParser(searchFullDOM bool) *Parser {
	return &Parser{
		searchFullDOM,
		&Movie{},
	}
}

// Parse creates Parser and invokes parsing and processing functions.
func (p *Parser) Parse(htmlData string) (*Movie, error) {
	p.movie = new(Movie)

	parsingResult, err := html.Parse(strings.NewReader(htmlData))
	if err != nil {
		return nil, errors.New("parsing error")
	}

	// If searchFullDOM then use function which traverses whole DOM
	// else only process a custom subset (see parser_test.go for more info)
	if p.searchFullDOM {
		p.movie, err = p.processAllNodes(parsingResult)
	} else {
		p.movie, err = p.processNodes(parsingResult)
	}
	if err != nil {
		return nil, errors.New("processing error")
	}
	return p.movie, nil
}

// processAllNodes processes parsed HTML and traverses full DOM to find relevant information.
// As an alternative to processNodes.
func (p *Parser) processAllNodes(rootNode *html.Node) (*Movie, error) {
	var processRecursive func(*html.Node)
	processRecursive = func(n *html.Node) {
		if isElementNode(n) {
			// check: title
			if n.Data == "h1" && nodeHasAttributes(n, "id", "aiv-content-title") {
				p.movie.Title = strings.TrimSpace(n.FirstChild.Data)
			}
			// check: releaseYear
			if n.Data == "span" && nodeHasAttributes(n, "class", "release-year") {
				p.movie.ReleaseYear = n.FirstChild.Data
			}
			// check: posterURL
			if n.Data == "div" && nodeHasAttributes(n, "class", "dp-meta-icon-container") {
				imgNodes, _ := getChildrenWithHTMLTag(n, "img")
				p.movie.PosterURL = getAttributeValuesWithName(&imgNodes[0], "src")[0]
			}
			// genre, regisseur, actor
			if n.Data == "table" && nodeHasAttributes(n, "class", "a-keyvalue a-horizontal-stripes a-align-top product-details-meta") {
				p.processDetailTable(n)
			}
			// similarMovies
			if n.Data == "ul" && nodeHasAttributes(n, "class", "shelf cf") {
				p.processSimilarMovies(n)
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			processRecursive(c)
		}
	}
	processRecursive(rootNode)

	return p.movie, nil
}

// processNodes processes parsed HTML and traverses specified nodes to find relevant information.
// As an alternative to processAllNodes.
func (p *Parser) processNodes(rootNode *html.Node) (*Movie, error) {
	// IMPROVE: access Nodes by pointer (call by reference) --> more efficient than copying.

	// IMPROVE: also get rating (X out of 5 stars), maybe amount of comments, fsk
	//output, err := html.Parse(strings.NewReader(bodyNode))

	// title
	htmlNodes, err := getChildrenWithHTMLTag(rootNode, "html")
	bodyNodes, err := getChildrenWithHTMLTag(&htmlNodes[0], "body")
	aPageNode, err := getChildWithHTMLTagAndAttribute(&bodyNodes[0], "div", "id", "a-page")
	dpMainNode, err := getChildWithHTMLTagAndAttribute(aPageNode, "div", "class", "dp-main")
	aivMainContentNode, err := getChildWithHTMLTagAndAttribute(dpMainNode, "div", "id", "aiv-main-content")
	dvDpTitleContentNode, err := getChildWithHTMLTagAndAttribute(aivMainContentNode, "div", "id", "dv-dp-title-content")
	aivContentTitleNode, err := getChildWithHTMLTagAndAttribute(dvDpTitleContentNode, "h1", "id", "aiv-content-title")
	p.movie.Title = strings.TrimSpace(aivContentTitleNode.FirstChild.Data)
	//log.Println("\nTITLE: ", p.movie.Title)

	// releaseYear
	releaseYearNode, err := getChildWithHTMLTagAndAttribute(aivContentTitleNode, "span", "class", "release-year")
	p.movie.ReleaseYear = releaseYearNode.FirstChild.Data
	//log.Println("\nRELEASE_YEAR: ", p.movie.ReleaseYear)

	// posterURL
	dvDpLeftContentNode, err := getChildWithHTMLTagAndAttribute(aivMainContentNode, "div", "id", "dv-dp-left-content")
	dpLeftMetaNode, err := getChildWithHTMLTagAndAttribute(dvDpLeftContentNode, "div", "class", "dp-left-meta")
	dpImgBracketNode, err := getChildWithHTMLTagAndAttribute(dpLeftMetaNode, "div", "class", "dp-img-bracket")
	dpMetaIconContainerNode, err := getChildWithHTMLTagAndAttribute(dpImgBracketNode, "div", "class", "dp-meta-icon-container")
	imgNodes, err := getChildrenWithHTMLTag(dpMetaIconContainerNode, "img")
	p.movie.PosterURL = getAttributeValuesWithName(&imgNodes[0], "src")[0]
	//log.Println("\nPOSTER_URL: ", p.movie.PosterURL)

	dvCenterFeatures, err := getChildWithHTMLTagAndAttribute(aPageNode, "div", "id", "dv-center-features")
	aivWrapper, err := getChildWithHTMLTagAndAttribute(dvCenterFeatures, "div", "class", "aiv-wrapper")
	aivContainerLimited, err := getChildWithHTMLTagAndAttribute(aivWrapper, "div", "class", "aiv-container-limited")
	tableNodes, err := getChildrenWithHTMLTag(aivContainerLimited, "table")
	//tBodyNode := tableNodes[0].FirstChild.NextSibling // from <table> to <tbody> via (empty) <thead>
	p.processDetailTable(&tableNodes[0])

	// useless, but need to handle "err"
	if err != nil {
		log.Fatalln("fail")
	}

	//log.Println("\nGENRES: ", p.movie.Genres)
	//log.Println("\nREGISSEURS: ", p.movie.Regisseurs)
	//log.Println("\nACTORS: ", p.movie.Actors)

	// similarMovies
	dvSims, err := getChildWithHTMLTagAndAttribute(aPageNode, "div", "id", "dv-sims")
	aivWrapper2, err := getChildWithHTMLTagAndAttribute(dvSims, "div", "class", "aiv-wrapper")
	aivContainerLimited2, err := getChildWithHTMLTagAndAttribute(aivWrapper2, "div", "class", "aiv-container-limited")
	dvWdgtCarousel, err := getChildWithHTMLTagAndAttribute(aivContainerLimited2, "div", "class", "dv-wdgt-carousel")
	ulNode := dvWdgtCarousel.FirstChild.NextSibling // also ErrorNodes in ul/li/inside li
	p.processSimilarMovies(ulNode)
	//log.Println("\nSIMILAR_IDS: ", p.movie.SimilarMovies)

	return p.movie, nil
}

// Processes parsed data for specific HTML-table which contains movie details.
func (p *Parser) processDetailTable(tableNode *html.Node) {
	tBodyNode := tableNode.FirstChild.NextSibling // from <table> to <tbody> via (empty) <thead>

	storeMovieDetail := func(n *html.Node, genres *[]string) {
		tdWrapper := n.FirstChild.NextSibling.NextSibling.NextSibling
		for n := tdWrapper.FirstChild.NextSibling; n != nil && isElementNode(n); n = n.NextSibling.NextSibling {
			*genres = append(*genres, n.FirstChild.Data)
		}
	}
	for child, i := tBodyNode.FirstChild, 0; child != nil; child, i = child.NextSibling.NextSibling, i+1 {
		/* TODO: Why are there so many ErrorNodes inside this table? have to call NextSibling very often in order to reach TDs
		Also need to skip two nodes in loop-header (NextSibling.NextSibling) in order to reach n+1th TR-node
		*/
		switch i {
		case 0: // genre info
			storeMovieDetail(child, &p.movie.Genres)
		case 1: // regisseur info
			storeMovieDetail(child, &p.movie.Regisseurs)
		case 2: // actors info (assuming every actor is wrapped by <a>-Tag)
			storeMovieDetail(child, &p.movie.Actors)
		}
	}
}

// Processes parsed data for specific HTML-list which contains similar movies.
func (p *Parser) processSimilarMovies(ulNode *html.Node) {
	for child, i := ulNode.FirstChild.NextSibling, 0; child != nil; child, i = child.NextSibling.NextSibling, i+1 {
		similarMovie := Movie{
			ID:        strings.Split(getAttributeValuesWithName(child.FirstChild.NextSibling, "href")[0], "/")[3],
			Title:     strings.Join(getAttributeValuesWithName(child.FirstChild.NextSibling.FirstChild.NextSibling, "alt"), " "), // alt Tag which holds movie title
			PosterURL: getAttributeValuesWithName(child.FirstChild.NextSibling.FirstChild.NextSibling, "src")[0],                 // href-Attribute and extracts ID from url
		}
		p.movie.SimilarMovies = append(p.movie.SimilarMovies, similarMovie)
	}
}

// Takes html.Node and returns children nodes which satify specified HTML-tag, if any.
func getChildrenWithHTMLTag(n *html.Node, tagName string) ([]html.Node, error) {
	result := make([]html.Node, 0, 1)
	for child := n.FirstChild; child != nil; child = child.NextSibling {
		if (child.Data == tagName) && isElementNode(child) {
			result = append(result, *child)
		}
	}
	if len(result) > 0 {
		return result, nil
	}
	return nil, errors.New("no children nodes with specified tag found")
}

// Takes html.Node and a filter. Returns children nodes which specified HTML-tag and attribute set.
// Will only return one node since this function is meant to find unique/distinct combinations.
func getChildWithHTMLTagAndAttribute(n *html.Node, tagName string, attrName string, attrValue string) (*html.Node, error) {
	for child := n.FirstChild; child != nil; child = child.NextSibling {
		if (child.Data == tagName) && isElementNode(child) {
			if result, err := contains(getAttributeValuesWithName(child, attrName), attrValue); result && err == nil {
				return child, nil
			}
		}
	}
	return nil, errors.New("no child node match with specified tag and attribute")
}

// Takes html.Node and attribute filter. Returns attribute values for specified attribute key inside html.Node.
func getAttributeValuesWithName(n *html.Node, attrKey string) []string {
	result := make([]string, 0, 1)
	for _, attr := range n.Attr {
		if attr.Key == attrKey {
			result = strings.Split(attr.Val, " ")
			break
		}
	}
	return result
}

// Checks if a list of elements contains a specific other element.
// Every type implements interface{}, so it can be used as generic here.
func contains(list interface{}, elem interface{}) (bool, error) {
	// Reflect parameters to test all types of parameters.
	// Type-Switch-Case and Type-Assertions fail as they need array/slice as first parameter.
	// So the call of this function with []string,string fails as []string doesn't respect []interface.
	v := reflect.ValueOf(list)
	for i := 0; i < v.Len(); i++ {
		if v.Index(i).Interface() == elem {
			return true, nil
		}
	}
	return false, errors.New("contains() failed")
}

// Checks if Y contains every element of X, so X is a subset of Y
func firstIsSubsetOfSecond(X, Y []string) bool {
	for _, elemX := range X {
		elemContained := false
		for _, elemY := range Y {
			if elemX == elemY {
				elemContained = true
				break
			}
		}
		if elemContained == false {
			return false
		}
	}
	return true
}

// Checks if an ElementNode contains specified attribute keys and values.
// Multiple attribute values can be passed as string with spaces between attributes.
func nodeHasAttributes(n *html.Node, attrKey string, attrVals string) bool {
	presentAttributes := getAttributeValuesWithName(n, attrKey)
	requiredAttributes := strings.Split(attrVals, " ")
	return firstIsSubsetOfSecond(requiredAttributes, presentAttributes)
}

// Small wrapper to check if a node is a TextNode.
func isTextNode(n *html.Node) bool {
	return n.Type == html.TextNode
}

// Small wrapper to check if a node is an ElementNode (= HTML-tag)
func isElementNode(n *html.Node) bool {
	return n.Type == html.ElementNode
}
