package main

import (
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
)

// Retriever specifies target domain and path.
type Retriever struct {
	queryURL *url.URL
}

// NewRetriever creates a Retriever and assigns the queryURL
func NewRetriever(queryURL *url.URL) *Retriever {
	return &Retriever{
		queryURL,
	}
}

// Retrieve creates Retriever and calls getHTML().
func (r *Retriever) Retrieve(id string) (string, error) {
	log.Println(r.queryURL, "IDDD:", id)
	res, err := http.Get(r.queryURL.String() + id)
	defer res.Body.Close()

	// IMPROVE: disable TLS so https-secured sites won't result in failure
	if err != nil || res.StatusCode != 200 {
		log.Println("http.Get error: ", err, "StatusCode:", res.StatusCode)
		return "", errors.New("http.GET error. Is specified URL valid?")
	}
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", errors.New("ioutil.ReadAll error")
	}
	return string(body), nil
}
